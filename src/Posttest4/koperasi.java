/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Posttest4;

import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Asus
 */
public class koperasi extends javax.swing.JFrame {

    private DefaultTableModel modelAnggota; //untuk membuat model pada tabel
    private DefaultTableModel modelPegawai; //untuk membuat model pada tabel
    private DefaultTableModel modelSimpan; //untuk membuat model pada tabel
    private DefaultTableModel modelPinjam; //untuk membuat model pada tabel
    private Connection con = koneksidb.getKoneksi(); //untuk mengambil koneksi 
    private Statement stt; //untuk eksekusi query database 
    private ResultSet rss; //untuk penampung data dari database 
    
    public koperasi() {
        initComponents();
    }

    private void InitTablesimpan(){ 
        modelSimpan = new DefaultTableModel();        
        modelSimpan.addColumn("NO"); 
        modelSimpan.addColumn("NO KTP");         
        modelSimpan.addColumn("NAMA"); 
        modelSimpan.addColumn("SIMPANAN");        
        jTableSimpan.setModel(modelSimpan); 
    }
    
    private void TampilDatasimpan() {
        try {
            int number = 0;
            String sql = "SELECT * FROM simpanpinjam";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()) {
                number++;
                Object[] obj = new Object[6];
                obj[0] = number;
                obj[1] = rss.getString("no_ktp");
                obj[2] = rss.getString("nama");
                obj[3] = rss.getInt("simpan");
                modelSimpan.addRow(obj);
            }
        } catch (SQLException e) {
        }
    }
    
    public boolean UbahDatasimpan(String ktp, int simpan) {
        try {
            String sql = "UPDATE simpanpinjam SET simpan = '"+simpan+"' WHERE no_ktp = '"+ktp+"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private void PencarianDataSimpan( String by, String cari){        
        try{ 
            int number = 0;
           String sql = "SELECT * FROM simpanpinjam WHERE "+by+" LIKE'%"+cari+"%';";            
           stt = con.createStatement(); 
           rss = stt.executeQuery(sql);            
           while(rss.next()){
               number++;
               Object[] data = new Object[4];                 
               data[0] = number; 
                data[1] = rss.getString("no_ktp");                 
                data[2] = rss.getString("nama"); 
                data[3] = rss.getInt("simpan");                 
                modelSimpan.addRow(data); 
            }             
        }catch(SQLException e){ 
                System.out.println(e.getMessage());          
        }    
    } 
    
    private void InitTablepinjam(){ 
        modelPinjam = new DefaultTableModel();        
        modelPinjam.addColumn("NO"); 
        modelPinjam.addColumn("NO KTP");         
        modelPinjam.addColumn("NAMA"); 
        modelPinjam.addColumn("PINJAMAN");        
        jTablePinjam.setModel(modelPinjam); 
    }
    
    private void TampilDatapinjam() {
        try {
            int number = 0;
            String sql = "SELECT * FROM simpanpinjam";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()) {
                number++;
                Object[] obj = new Object[6];
                obj[0] = number;
                obj[1] = rss.getString("no_ktp");
                obj[2] = rss.getString("nama");
                obj[3] = rss.getInt("pinjam");
                modelPinjam.addRow(obj);
            }
        } catch (SQLException e) {
        }
    }
    
    public boolean UbahDatapinjam(String ktp, int pinjam) {
        try {
            String sql = "UPDATE simpanpinjam SET pinjam = '"+pinjam+"' WHERE no_ktp = '"+ktp+"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    private void PencarianDataPinjam( String by, String cari){        
        try{ 
            int number = 0;
           String sql = "SELECT * FROM simpanpinjam WHERE "+by+" LIKE'%"+cari+"%';";            
           stt = con.createStatement(); 
           rss = stt.executeQuery(sql);            
           while(rss.next()){
               number++;
               Object[] data = new Object[4];                 
               data[0] = number; 
                data[1] = rss.getString("no_ktp");                 
                data[2] = rss.getString("nama"); 
                data[3] = rss.getInt("pinjam");                 
                modelPinjam.addRow(data); 
            }             
        }catch(SQLException e){ 
                System.out.println(e.getMessage());          
        }    
    } 
    /*
        Anggota
    */
    private void InitTableAnggota(){ 
        modelAnggota = new DefaultTableModel();        
        modelAnggota.addColumn("NO"); 
        modelAnggota.addColumn("NO KTP");         
        modelAnggota.addColumn("NAMA"); 
        modelAnggota.addColumn("JENIS KELAMIN");         
        modelAnggota.addColumn("TELEPON");         
        modelAnggota.addColumn("ALAMAT");         
        jTableAnggota.setModel(modelAnggota); 
    }
    
    private void TampilDataanggota() {
        try {
            int number = 0;
            String sql = "SELECT * FROM anggota";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()) {
                number++;
                Object[] obj = new Object[6];
                obj[0] = number;
                obj[1] = rss.getString("no_ktp");
                obj[2] = rss.getString("nama");
                obj[3] = rss.getString("jenis_kelamin");
                obj[4] = rss.getInt("no_hp");
                obj[5] = rss.getString("alamat");
                modelAnggota.addRow(obj);
            }
        } catch (SQLException e) {
        }
    }
    
    private void TambahDataanggota(String ktp, String nama, String jk, int hp, String alamat) {
        try {
            String sql = "INSERT INTO anggota VALUES(NULL,'"+ktp+"','"+nama+"','"+jk+"','"+hp+"','"+alamat+"');";
            String sql2 = "INSERT INTO simpanpinjam VALUES(NULL,'"+ktp+"','"+nama+"','0','0');";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            stt.executeUpdate(sql2);
            InitTableAnggota();
            TampilDataanggota();
            InitTablesimpan();
            TampilDatasimpan();
            InitTablepinjam();
            TampilDatapinjam();
        } catch (SQLException e) {
        }
    }
    
    public boolean UbahDataanggota(String ktp, String nama, String jk, int hp, String alamat, String lama) {
        try {
            String sql = "UPDATE anggota SET no_ktp = '"+ktp+"', nama = '"+nama+"', jenis_kelamin = '"+jk+"', no_hp = "+hp+", alamat = '"+alamat+"' WHERE no_ktp = '"+lama+"';";
            String sql2 = "UPDATE simpanpinjam SET no_ktp = '"+ktp+"', nama = '"+nama+"' WHERE no_ktp = '"+lama+"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            stt.executeUpdate(sql2);
            InitTablesimpan();
            TampilDatasimpan();
            InitTablepinjam();
            TampilDatapinjam();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean HapusDataanggota(String ktp) {
        try {
            String sql = "DELETE FROM anggota WHERE no_ktp = '" + ktp +"';";
            String sql2 = "DELETE FROM simpanpinjam WHERE no_ktp = '" + ktp +"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            stt.executeUpdate(sql2);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
    
    /*
        PEGAWAI
    */
    
    private void InitTablepegawai(){ 
        modelPegawai = new DefaultTableModel();        
        modelPegawai.addColumn("NO"); 
        modelPegawai.addColumn("NO KTP");         
        modelPegawai.addColumn("NAMA"); 
        modelPegawai.addColumn("JENIS KELAMIN");         
        modelPegawai.addColumn("TELEPON");         
        modelPegawai.addColumn("ALAMAT");         
        jTablePegawai.setModel(modelPegawai); 
    }
    
    private void TampilDatapegawai() {
        try {
            int number = 0;
            String sql = "SELECT * FROM pegawai";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()) {
                number++;
                Object[] obj = new Object[6];
                obj[0] = number;
                obj[1] = rss.getString("no_ktp");
                obj[2] = rss.getString("nama");
                obj[3] = rss.getString("jenis_kelamin");
                obj[4] = rss.getInt("no_hp");
                obj[5] = rss.getString("alamat");
                modelPegawai.addRow(obj);
            }
        } catch (SQLException e) {
        }
    }
    
    private void TambahDatapegawai(String ktp, String nama, String jk, int hp, String alamat) {
        try {
            String sql = "INSERT INTO pegawai VALUES(NULL,'"+ktp+"','"+nama+"','"+jk+"','"+hp+"','"+alamat+"');";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            InitTablepegawai();
            TampilDatapegawai();
        } catch (SQLException e) {
        }
    }
    
    public boolean UbahDatapegawai(String ktp, String nama, String jk, int hp, String alamat, String lama) {
        try {
            String sql = "UPDATE pegawai SET no_ktp = '"+ktp+"', nama = '"+nama+"', jenis_kelamin = '"+jk+"', no_hp = "+hp+", alamat = '"+alamat+"' WHERE no_ktp = '"+lama+"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
    
    public boolean HapusDatapegawai(String ktp) {
        try {
            String sql = "DELETE FROM pegawai WHERE no_ktp = '" + ktp +"';";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kelamin = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        lblktp = new javax.swing.JLabel();
        lblnama = new javax.swing.JLabel();
        lbljk = new javax.swing.JLabel();
        lblhp = new javax.swing.JLabel();
        lblalamat = new javax.swing.JLabel();
        txtktp = new javax.swing.JTextField();
        txtnama = new javax.swing.JTextField();
        btnpr = new javax.swing.JRadioButton();
        btnlk = new javax.swing.JRadioButton();
        txthp = new javax.swing.JTextField();
        txtalamat = new javax.swing.JTextField();
        btntambah = new javax.swing.JButton();
        btnubah = new javax.swing.JButton();
        btnhapus = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAnggota = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lblktp1 = new javax.swing.JLabel();
        lblnama1 = new javax.swing.JLabel();
        lbljk1 = new javax.swing.JLabel();
        lblhp1 = new javax.swing.JLabel();
        lblalamat1 = new javax.swing.JLabel();
        txtktp1 = new javax.swing.JTextField();
        txtnama1 = new javax.swing.JTextField();
        btnpr1 = new javax.swing.JRadioButton();
        btnlk1 = new javax.swing.JRadioButton();
        txthp1 = new javax.swing.JTextField();
        txtalamat1 = new javax.swing.JTextField();
        btntambah1 = new javax.swing.JButton();
        btnubah1 = new javax.swing.JButton();
        btnhapus1 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTablePegawai = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblktp2 = new javax.swing.JLabel();
        lblnama2 = new javax.swing.JLabel();
        txtktp2 = new javax.swing.JTextField();
        txtnama2 = new javax.swing.JTextField();
        lblsimpan = new javax.swing.JLabel();
        jLabelSimpanan = new javax.swing.JLabel();
        txtsimpan = new javax.swing.JTextField();
        btntambah2 = new javax.swing.JButton();
        btnreset = new javax.swing.JButton();
        lblnarik = new javax.swing.JLabel();
        txtnarik = new javax.swing.JTextField();
        btnambil2 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableSimpan = new javax.swing.JTable();
        lblcari = new javax.swing.JLabel();
        txtcari = new javax.swing.JTextField();
        cblist = new javax.swing.JComboBox<>();
        jPanel9 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtktp3 = new javax.swing.JTextField();
        txtnama3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabelTotalPinjam = new javax.swing.JLabel();
        txtPinjaman = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtBayaran = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldcari = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePinjam = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 153, 51));

        jPanel3.setBackground(new java.awt.Color(102, 153, 0));

        lblktp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblktp.setText("No KTP");

        lblnama.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblnama.setText("Nama");

        lbljk.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbljk.setText("Jenis Kelamin");

        lblhp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblhp.setText("No HP");

        lblalamat.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblalamat.setText("Alamat");

        kelamin.add(btnpr);
        btnpr.setText("Perempuan");

        kelamin.add(btnlk);
        btnlk.setText("Laki-laki");

        btntambah.setText("Tambah");
        btntambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntambahActionPerformed(evt);
            }
        });

        btnubah.setText("Ubah");
        btnubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnubahActionPerformed(evt);
            }
        });

        btnhapus.setText("Hapus");
        btnhapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhapusActionPerformed(evt);
            }
        });

        jTableAnggota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTableAnggota.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableAnggotaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableAnggota);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblktp)
                            .addComponent(lblnama)
                            .addComponent(lbljk)
                            .addComponent(lblhp)
                            .addComponent(lblalamat))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtalamat, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnpr)
                                .addGap(18, 18, 18)
                                .addComponent(btnlk))
                            .addComponent(txtnama)
                            .addComponent(txthp)
                            .addComponent(txtktp))
                        .addGap(39, 39, 39))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(btntambah)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnubah)
                        .addGap(68, 68, 68)
                        .addComponent(btnhapus)
                        .addGap(80, 80, 80))))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblktp)
                    .addComponent(txtktp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lblnama))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtnama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lbljk)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblhp))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnpr)
                            .addComponent(btnlk))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txthp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtalamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblalamat))))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnubah)
                    .addComponent(btnhapus)
                    .addComponent(btntambah))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Anggota", jPanel3);

        jPanel6.setBackground(new java.awt.Color(153, 204, 0));

        lblktp1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblktp1.setText("No KTP");

        lblnama1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblnama1.setText("Nama");

        lbljk1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbljk1.setText("Jenis Kelamin");

        lblhp1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblhp1.setText("No HP");

        lblalamat1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblalamat1.setText("Alamat");

        kelamin.add(btnpr1);
        btnpr1.setText("Perempuan");

        kelamin.add(btnlk1);
        btnlk1.setText("Laki-laki");

        btntambah1.setText("Tambah");
        btntambah1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntambah1ActionPerformed(evt);
            }
        });

        btnubah1.setText("Ubah");
        btnubah1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnubah1ActionPerformed(evt);
            }
        });

        btnhapus1.setText("Hapus");
        btnhapus1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhapus1ActionPerformed(evt);
            }
        });

        jTablePegawai.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTablePegawai.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTablePegawaiMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTablePegawai);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(lbljk1)
                                .addComponent(lblktp1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblnama1, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblhp1, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(lblalamat1))
                        .addGap(66, 66, 66)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(btnpr1)
                                .addGap(48, 48, 48)
                                .addComponent(btnlk1))
                            .addComponent(txtnama1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txthp1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtalamat1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtktp1, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(btntambah1)
                        .addGap(73, 73, 73)
                        .addComponent(btnubah1)
                        .addGap(65, 65, 65)
                        .addComponent(btnhapus1))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblktp1)
                    .addComponent(txtktp1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblnama1)
                    .addComponent(txtnama1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbljk1)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnpr1)
                        .addComponent(btnlk1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblhp1)
                    .addComponent(txthp1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblalamat1)
                    .addComponent(txtalamat1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnhapus1)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnubah1)
                        .addComponent(btntambah1)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Pegawai", jPanel4);

        jPanel7.setBackground(new java.awt.Color(0, 102, 51));

        jPanel8.setBackground(new java.awt.Color(0, 153, 153));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Dana Simpan");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(181, 181, 181)
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblktp2.setText("No KTP");

        lblnama2.setText("Nama");

        lblsimpan.setText("Uang Simpan");

        jLabelSimpanan.setText("Rp. 0");

        btntambah2.setText("Tambah");
        btntambah2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntambah2ActionPerformed(evt);
            }
        });

        btnreset.setText("Reset");
        btnreset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnresetActionPerformed(evt);
            }
        });

        lblnarik.setText("Penarikan");

        btnambil2.setText("Ambil");
        btnambil2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnambil2ActionPerformed(evt);
            }
        });

        jTableSimpan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTableSimpan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableSimpanMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTableSimpan);

        lblcari.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblcari.setText("Cari");

        txtcari.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtcariCaretUpdate(evt);
            }
        });
        txtcari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcariActionPerformed(evt);
            }
        });

        cblist.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "no_ktp", "nama" }));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(txtsimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(btnreset)
                .addGap(36, 36, 36)
                .addComponent(txtnarik, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(74, 74, 74)
                                .addComponent(lblcari, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtcari, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(btntambah2)))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addComponent(cblist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(btnambil2))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabelSimpanan))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lblktp2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtktp2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lblnama2)
                                .addGap(62, 62, 62)
                                .addComponent(txtnama2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(lblsimpan)
                        .addGap(213, 213, 213)
                        .addComponent(lblnarik)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblktp2)
                    .addComponent(txtktp2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnama2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblnama2))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblnarik)
                    .addComponent(lblsimpan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelSimpanan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtsimpan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnarik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnreset))
                .addGap(3, 3, 3)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnambil2)
                    .addComponent(btntambah2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cblist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblcari))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Data Simpan", jPanel5);

        jPanel11.setBackground(new java.awt.Color(153, 204, 0));

        jPanel10.setBackground(new java.awt.Color(0, 204, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Data Pinjam");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("No KTP");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nama");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Uang Pinjam");

        jLabelTotalPinjam.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelTotalPinjam.setText("Rp. 0");

        txtPinjaman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPinjamanActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Bayar ");

        jButton1.setText("Reset");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Tambah");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Submit");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Cari");

        jTextFieldcari.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextFieldcariCaretUpdate(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "no_ktp", "nama" }));

        jTablePinjam.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTablePinjam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTablePinjamMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTablePinjam);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(txtPinjaman, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addComponent(jButton1)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(txtBayaran)
                        .addGap(123, 123, 123))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jButton3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addComponent(jLabel9)
                .addGap(32, 32, 32)
                .addComponent(jTextFieldcari, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(174, 174, 174)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtnama3, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtktp3, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelTotalPinjam)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel2)
                                                .addComponent(jLabel3))
                                            .addGroup(jPanel11Layout.createSequentialGroup()
                                                .addComponent(jLabel4)
                                                .addGap(7, 7, 7)))
                                        .addGap(160, 160, 160)
                                        .addComponent(jLabel8)))))
                        .addGap(0, 95, Short.MAX_VALUE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtktp3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtnama3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelTotalPinjam)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jButton1))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBayaran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPinjaman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2)
                            .addComponent(jButton3))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTextFieldcari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Data Pinjam", jPanel9);

        jPanel13.setBackground(new java.awt.Color(0, 153, 51));

        jButton4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton4.setText("LOGOUT");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(jButton4)
                .addContainerGap(196, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(157, 157, 157)
                .addComponent(jButton4)
                .addContainerGap(190, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("LOGOUT", jPanel12);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel10.setText("KOPERASI SIMPAN PINJAM");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        InitTableAnggota();
        InitTablepegawai();
        InitTablesimpan();
        InitTablepinjam();
        TampilDataanggota();
        TampilDatapegawai();
        TampilDatasimpan();
        TampilDatapinjam();
        
    }//GEN-LAST:event_formComponentShown

    private void btntambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntambahActionPerformed
        String jk;
        if(btnpr.isSelected()) {
            jk = btnpr.getText();
        }
        else {
            jk = btnlk.getText();
        }
        TambahDataanggota(txtktp.getText(), txtnama.getText(), jk, Integer.parseInt(txthp.getText()), txtalamat.getText());
    }//GEN-LAST:event_btntambahActionPerformed

    private void btnubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnubahActionPerformed
        int baris = jTableAnggota.getSelectedRow();
        String ktp = jTableAnggota.getValueAt(baris, 1).toString();
        String jk;
        if(btnpr.isSelected()) {
            jk = btnpr.getText();
        }
        else {
            jk = btnlk.getText();
        }
        if(UbahDataanggota(txtktp.getText(), txtnama.getText(), jk, Integer.parseInt(txthp.getText()), txtalamat.getText(), ktp)) {
            JOptionPane.showMessageDialog(this, "Berhasil Diubah");
        }
        else {
            JOptionPane.showMessageDialog(this, "Gagal Diubah");
        }
        InitTableAnggota();
        TampilDataanggota();
    }//GEN-LAST:event_btnubahActionPerformed

    private void btnhapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhapusActionPerformed
        int baris = jTableAnggota.getSelectedRow();
        String ktp = jTableAnggota.getValueAt(baris, 1).toString();
        HapusDataanggota(ktp);
        InitTableAnggota();
        TampilDataanggota();
        InitTablesimpan();
        TampilDatasimpan();
        InitTablepinjam();
        TampilDatapinjam();
    }//GEN-LAST:event_btnhapusActionPerformed

    private void jTableAnggotaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAnggotaMouseClicked
        int baris = jTableAnggota.getSelectedRow();
        txtktp.setText(jTableAnggota.getValueAt(baris, 1).toString());
        txtnama.setText(jTableAnggota.getValueAt(baris, 2).toString());
        if(jTableAnggota.getValueAt(baris, 3).toString().equals("Perempuan")) {
            btnpr.setSelected(true);
        }
        else {
            btnlk.setSelected(true);
        }
        txthp.setText(jTableAnggota.getValueAt(baris, 4).toString());
        txtalamat.setText(jTableAnggota.getValueAt(baris, 5).toString());
    }//GEN-LAST:event_jTableAnggotaMouseClicked

    private void btntambah1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntambah1ActionPerformed
        String jk;
        if(btnpr1.isSelected()) {
            jk = btnpr1.getText();
        }
        else {
            jk = btnlk1.getText();
        }
        TambahDatapegawai(txtktp1.getText(), txtnama1.getText(), jk, Integer.parseInt(txthp1.getText()), txtalamat1.getText());
    }//GEN-LAST:event_btntambah1ActionPerformed

    private void btnubah1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnubah1ActionPerformed
        int baris = jTablePegawai.getSelectedRow();
        String ktp = jTablePegawai.getValueAt(baris, 1).toString();
        String jk;
        if(btnpr1.isSelected()) {
            jk = btnpr1.getText();
        }
        else {
            jk = btnlk1.getText();
        }
        if(UbahDatapegawai(txtktp1.getText(), txtnama1.getText(), jk, Integer.parseInt(txthp1.getText()), txtalamat1.getText(), ktp)) {
            JOptionPane.showMessageDialog(this, "Berhasil Diubah");
        }
        else {
            JOptionPane.showMessageDialog(this, "Gagal Diubah");
        }
        InitTablepegawai();
        TampilDatapegawai();
    }//GEN-LAST:event_btnubah1ActionPerformed

    private void btnhapus1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhapus1ActionPerformed
        int baris = jTablePegawai.getSelectedRow();
        String ktp = jTablePegawai.getValueAt(baris, 1).toString();
        HapusDatapegawai(ktp);
        InitTablepegawai();
        TampilDatapegawai();
    }//GEN-LAST:event_btnhapus1ActionPerformed

    private void jTablePegawaiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePegawaiMouseClicked
        int baris = jTablePegawai.getSelectedRow();
        txtktp1.setText(jTablePegawai.getValueAt(baris, 1).toString());
        txtnama1.setText(jTablePegawai.getValueAt(baris, 2).toString());
        if(jTablePegawai.getValueAt(baris, 3).toString().equals("Perempuan")) {
            btnpr1.setSelected(true);
        }
        else {
            btnlk1.setSelected(true);
        }
        txthp1.setText(jTablePegawai.getValueAt(baris, 4).toString());
        txtalamat1.setText(jTablePegawai.getValueAt(baris, 5).toString());
    }//GEN-LAST:event_jTablePegawaiMouseClicked

    private void btntambah2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntambah2ActionPerformed
        int baris = jTableSimpan.getSelectedRow();
        String ktp = jTableSimpan.getValueAt(baris, 1).toString();
        int totalsimpan = Integer.parseInt(jTableSimpan.getValueAt(baris, 3).toString()) + Integer.parseInt(txtsimpan.getText());
        jLabelSimpanan.setText("Rp. "+totalsimpan);
        UbahDatasimpan(ktp, totalsimpan);
        InitTablesimpan();
        TampilDatasimpan();
    }//GEN-LAST:event_btntambah2ActionPerformed

    private void btnambil2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnambil2ActionPerformed
        int baris = jTableSimpan.getSelectedRow();
        String ktp = jTableSimpan.getValueAt(baris, 1).toString();
        int totalsimpan = Integer.parseInt(jTableSimpan.getValueAt(baris, 3).toString()) - Integer.parseInt(txtnarik.getText());
        jLabelSimpanan.setText("Rp. "+totalsimpan);
        UbahDatasimpan(ktp, totalsimpan);
        InitTablesimpan();
        TampilDatasimpan();
    }//GEN-LAST:event_btnambil2ActionPerformed

    private void btnresetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnresetActionPerformed
        txtktp2.setText("");
        txtnama2.setText("");
        jLabelSimpanan.setText("Rp. 0");
        txtsimpan.setText("");
        txtnarik.setText("");
    }//GEN-LAST:event_btnresetActionPerformed

    private void jTableSimpanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableSimpanMouseClicked
        int baris = jTableSimpan.getSelectedRow();
        txtktp2.setText(jTableSimpan.getValueAt(baris, 1).toString());
        txtnama2.setText(jTableSimpan.getValueAt(baris, 2).toString());
        jLabelSimpanan.setText("Rp. "+jTableSimpan.getValueAt(baris, 3).toString());
    }//GEN-LAST:event_jTableSimpanMouseClicked

    private void txtPinjamanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPinjamanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPinjamanActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        int baris = jTablePinjam.getSelectedRow();
        String ktp = jTablePinjam.getValueAt(baris, 1).toString();
        int totalpinjam = Integer.parseInt(jTablePinjam.getValueAt(baris, 3).toString()) + Integer.parseInt(txtPinjaman.getText());
        jLabelTotalPinjam.setText("Rp. "+totalpinjam);
        UbahDatapinjam(ktp, totalpinjam);
        InitTablepinjam();
        TampilDatapinjam();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int baris = jTablePinjam.getSelectedRow();
        String ktp = jTablePinjam.getValueAt(baris, 1).toString();
        int totalpinjam = Integer.parseInt(jTablePinjam.getValueAt(baris, 3).toString()) - Integer.parseInt(txtBayaran.getText());
        jLabelTotalPinjam.setText("Rp. "+totalpinjam);
        UbahDatapinjam(ktp, totalpinjam);
        InitTablepinjam();
        TampilDatapinjam();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTablePinjamMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePinjamMouseClicked
        int baris = jTablePinjam.getSelectedRow();
        txtktp3.setText(jTablePinjam.getValueAt(baris, 1).toString());
        txtnama3.setText(jTablePinjam.getValueAt(baris, 2).toString());
        jLabelTotalPinjam.setText("Rp. "+jTablePinjam.getValueAt(baris, 3).toString());
    }//GEN-LAST:event_jTablePinjamMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        txtktp3.setText("");
        txtnama3.setText("");
        jLabelTotalPinjam.setText("Rp. 0");
        txtBayaran.setText("");
        txtPinjaman.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtcariCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtcariCaretUpdate
       InitTablesimpan();
       if(txtcari.getText().length() == 0) {
           TampilDatasimpan();
       }
       else {
           PencarianDataSimpan(cblist.getSelectedItem().toString(), txtcari.getText());
       }
    }//GEN-LAST:event_txtcariCaretUpdate

    private void jTextFieldcariCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextFieldcariCaretUpdate
        InitTablepinjam();
       if(jTextFieldcari.getText().length() == 0) {
           TampilDatapinjam();
       }
       else {
           PencarianDataPinjam(jComboBox1.getSelectedItem().toString(), jTextFieldcari.getText());
       }
    }//GEN-LAST:event_jTextFieldcariCaretUpdate

    private void txtcariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcariActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        JOptionPane.showMessageDialog(this, "Anda Logout Berhasil");
        dispose();
        new login().setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(koperasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(koperasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(koperasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(koperasi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new koperasi().setVisible(true);
                    new login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnambil2;
    private javax.swing.JButton btnhapus;
    private javax.swing.JButton btnhapus1;
    private javax.swing.JRadioButton btnlk;
    private javax.swing.JRadioButton btnlk1;
    private javax.swing.JRadioButton btnpr;
    private javax.swing.JRadioButton btnpr1;
    private javax.swing.JButton btnreset;
    private javax.swing.JButton btntambah;
    private javax.swing.JButton btntambah1;
    private javax.swing.JButton btntambah2;
    private javax.swing.JButton btnubah;
    private javax.swing.JButton btnubah1;
    private javax.swing.JComboBox<String> cblist;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelSimpanan;
    private javax.swing.JLabel jLabelTotalPinjam;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableAnggota;
    private javax.swing.JTable jTablePegawai;
    private javax.swing.JTable jTablePinjam;
    private javax.swing.JTable jTableSimpan;
    private javax.swing.JTextField jTextFieldcari;
    private javax.swing.ButtonGroup kelamin;
    private javax.swing.JLabel lblalamat;
    private javax.swing.JLabel lblalamat1;
    private javax.swing.JLabel lblcari;
    private javax.swing.JLabel lblhp;
    private javax.swing.JLabel lblhp1;
    private javax.swing.JLabel lbljk;
    private javax.swing.JLabel lbljk1;
    private javax.swing.JLabel lblktp;
    private javax.swing.JLabel lblktp1;
    private javax.swing.JLabel lblktp2;
    private javax.swing.JLabel lblnama;
    private javax.swing.JLabel lblnama1;
    private javax.swing.JLabel lblnama2;
    private javax.swing.JLabel lblnarik;
    private javax.swing.JLabel lblsimpan;
    private javax.swing.JTextField txtBayaran;
    private javax.swing.JTextField txtPinjaman;
    private javax.swing.JTextField txtalamat;
    private javax.swing.JTextField txtalamat1;
    private javax.swing.JTextField txtcari;
    private javax.swing.JTextField txthp;
    private javax.swing.JTextField txthp1;
    private javax.swing.JTextField txtktp;
    private javax.swing.JTextField txtktp1;
    private javax.swing.JTextField txtktp2;
    private javax.swing.JTextField txtktp3;
    private javax.swing.JTextField txtnama;
    private javax.swing.JTextField txtnama1;
    private javax.swing.JTextField txtnama2;
    private javax.swing.JTextField txtnama3;
    private javax.swing.JTextField txtnarik;
    private javax.swing.JTextField txtsimpan;
    // End of variables declaration//GEN-END:variables
}
